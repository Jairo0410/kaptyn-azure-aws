const socket = io();

function sendContent() {
    let content = document.getElementById('content').value;

    socket.emit('create-post', {
        content : content
    });

    console.log(content);
}

socket.on('arrived-post', function(data){
    console.log('Arrived post:', data);
});
