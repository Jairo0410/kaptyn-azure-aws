var AWS = require('aws-sdk');

AWS.config.update({region: 'us-east-2'});
let SNS = new AWS.SNS({apiVersion: '2010-03-31'});

class AWS_SNS {
    constructor() {
    }

    sendPost(post) {
        // Create publish parameters
        let params = {
            Message: post,
            TopicArn: 'arn:aws:sns:us-east-2:277939740167:kaptyn-mailinglist'
        };

        // Create promise and SNS service object
        let publishTextPromise = SNS.publish(params).promise();
        return publishTextPromise;
    }
}

module.exports = AWS_SNS;
